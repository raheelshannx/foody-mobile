import React from "react";
import CartDetailArea from "./partials/Home/CartDetailArea";
import WhatsappIcon from "./partials/Home/WhatsappIcon";
import { useSelector } from "react-redux";

function FixedButtonArea() {
  const settings = useSelector((state) => state.webSetting);

  const whatsApp = (settings) => {
    if (settings.whatsapp_number != null) {
      return <WhatsappIcon number={settings.whatsapp_number} />;
    } else {
      return "";
    }
  };

  return (
    <div className="btn_area">
      <CartDetailArea />
      {whatsApp(settings)}
    </div>
  );
}

export default FixedButtonArea;
