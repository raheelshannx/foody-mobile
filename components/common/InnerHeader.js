import React from "react";

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }
function InnerHeader() {
    return (
        <header className="inner_header">
        <div className="d-flex justify-content-between">
            <span onClick={openNav}>
                <i>
                    <img loading="lazy" src="assets/img/menu_icon.svg" alt="menu" />
                </i>
            </span>
        </div>
    </header>
    );
}
export default InnerHeader;