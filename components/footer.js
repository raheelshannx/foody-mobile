import React from "react";
import useLanguage from "../services/useLanguage";
import { useSelector } from "react-redux";
import Link from "next/link";

function Footer() {
  const lang = useLanguage();
  const pages = useSelector((state) => state.pages);
  const store = useSelector((state) => state.store);
  const socialLinks = useSelector((state) => state.socialLinks);

  return (
    <footer>
      <ul>
        {pages.map((page) => {
          let link = `page?slug=${page.slug}`;
          return (
            <li key={page.slug}>
              <Link href={link}>
                <a href="#">{page[lang]["name"]}</a>
              </Link>
            </li>
          );
        })}
      </ul>
      <p>{store.phone}</p>
      <p>{store.email}</p>
      <p>{store.address}</p>

      <div className="social_icons">
        {socialLinks.map((socialLink) => {
          return (
            <a href={socialLink.link} key={socialLink.icon} target="_blank">
              <i className={socialLink.icon} />
            </a>
          );
        })}
      </div>
    </footer>
  );
}

export default Footer;
