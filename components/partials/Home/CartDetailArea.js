import React from "react";
import Link from "next/link";
import useCurrency from "../../../services/useCurrency";
import { useSelector } from "react-redux";
import { useState, useEffect } from "react";

function CartDetailArea() {
  const [total, setTotal] = useState(0);
  const [items, setItems] = useState(0);
  const currency = useCurrency();
  const cart = useSelector((state) => state.cart);
  const settings = useSelector((state) => state.webSetting);
  let styleClass = `col-md-8`;//${settings.whatsapp_number == null ? 12 : 8}

  useEffect(() => {
    setTimeout(() => {
      setItems(cart.items.length);
      setTotal(cart.total);
    },500)
  }, [total,items,cart]);


  return (
    <div className={styleClass}>
      <span className="qty">{cart.items.length}</span> {currency} : {cart.total}
      <span className="cart">
        <Link href="/cart">
          <a href="#">
            <span className="iconify" data-icon="mdi:cart-arrow-right" />
          </a>
        </Link>
      </span>
    </div>
  );
}

export default CartDetailArea;
