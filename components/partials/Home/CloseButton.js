import React from "react";

function CloseButton() {
    return (
        <div>
            <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
            >
                <span aria-hidden="true">×</span>
            </button>
        </div>
    );
}

export default CloseButton;
