import CategorySection from "../index3/CategorySection"
import { useSelector } from "react-redux";

function Home3Products({products}){
    const categories = useSelector((state) => state.categories);
    return(
        <div className="products_area">
                <div id="accordion" className="accordion  mt-4 mb-4">
                    <div className="card  mb-0">
                        <CategorySection parent={`#accordion`} categories={categories} products={products}/>
                    </div>
                </div>
            </div>
    );
}
export default Home3Products;;