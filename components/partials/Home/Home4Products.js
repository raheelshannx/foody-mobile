import ProductSection from "../index4/ProductSection";

function Home4Products({products}){
    return(
        <div className="home2_products_area">
        <div className="products_row">
          <ProductSection products={products}/>
          <hr />
        </div>
      </div>
    );
}
export default Home4Products;