import MenuProduct from "../index5/MenuProduct";
import useTranslation from "../../../services/useTranslation";
import { useSelector } from "react-redux";

function Home5Products({ products }) {
  const { Menu } = useTranslation();
  const categories = useSelector((state) => state.categories);
  return (
    <div className="home3_products_area">
      <h3 className="p-3">{Menu}</h3>
      <ul className="inner_pro">

        {
          categories.map(category => {
            let filteredProducts = products.filter(prod => {
              return prod.category_id == category.id;
            });
            if (filteredProducts != null && filteredProducts.length > 0) {
              return (
                <MenuProduct data={category} products={filteredProducts} />
              )
            }
          })
        }

      </ul>
    </div>
  );
}
export default Home5Products;