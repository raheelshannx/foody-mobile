import React from "react";
import { useSelector } from "react-redux";
import TabList from "./TabList";
import DeliveryAddressList from "./DeliveryAddressList";
import PickUpAddressList from "./PickUpAddressList";

function PopUp({setLocation,setPickupTrue}) {
    const outlets = useSelector((state) => state.outlets);
    const areas = useSelector((state) => state.areas);
    return (
        <div className="modal fade delivery_pickup_opup" id="pickup_opup" tabIndex={-1} role="dialog" aria-hidden="true" >
            <div className="modal-dialog modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-body">
                        <TabList />
                        <div className="tab-content" id="pills-tabContent">
                            <DeliveryAddressList setLocation={setLocation} setPickupTrue={setPickupTrue}/>
                            <PickUpAddressList setLocation={setLocation} setPickupTrue={setPickupTrue}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PopUp;
