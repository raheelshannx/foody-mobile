import React from "react";
import ProductImage from "./ProductImage";
import ProductName from "./ProductName";
import ProductDescription from "./ProductDescription";
import SelectAddOn from "./SelectAddOn";
import SpecialNotes from "./SpecialNotes";

function ProductDetail() {
  return (
    <div className="product_detail">
      <ProductImage />
      <ProductName />
      <ProductDescription />
      <div className="extra_selection">
        <div id="accordion2" className="accordion  mt-4 mb-4">
          <div className="card  mb-0">

            {
              [1, 2,].map(productdetail => {
                return (
                  <SelectAddOn  key={productdetail}/>
                )
              })
            }

          </div>
        </div>
        <SpecialNotes />
      </div>
    </div>
  );
}

export default ProductDetail;
