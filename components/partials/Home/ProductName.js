import React from "react";

function ProductName() {
    return (
        <div className="d-flex p-3 rounded grey m-3 font-bold justify-content-between">
            <span>Karahi Chicken</span>
            <span>KWD 30:00</span>
        </div>
    );
}

export default ProductName;
