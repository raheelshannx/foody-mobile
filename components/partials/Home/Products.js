import React from "react";
import ItemImage from "./ItemImage";
import ItemDetail from "./ItemDetail";
import ShowCart from "./ShowCart";
import ShowQuantity from "./ShowQuantity";
import useLanguage from "../../../services/useLanguage";
import useCurrency from "../../../services/useCurrency";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setPopup } from "../../../redux/actions/popupAction";
import { discardAddons } from "../../../redux/actions/addonAction";

function Products({ product }) {
  const lang = useLanguage();
  const [show, setShow] = useState(false);
  const dispatch = useDispatch();
  const currency = useCurrency();
  const store = useSelector((state) => state.store);

  const toggle = () => setShow((o) => !o);

  const setPopupData = () => {
    // product[lang].addons.map((addon) => {
    //   addon.items.map((item) => {
    //     item.selected = false;
    //     return item;
    //   });
    //   addon.complete = false;
    //   return addon;
    // });

    dispatch(setPopup(product));
    dispatch(discardAddons());
  };

  const renderControl = () => {
    if (product[lang].addons.length > 0) {
      return (
        <a
          href="#/"
          className="product_detail_ff"
          data-toggle="modal"
          data-target="#item_detail_popup"
          onClick={setPopupData}
        >
          <i className="icon-arrow-right"></i>
        </a>
      );
    } else {
      return (
        <a href="#/" title="Cart" onClick={toggle}>
          <img loading="lazy" src="/assets/img/cart_icon.svg" />
        </a>
      );
    }
  };

  return (
    <div className="row align-items-center">
      <div className="col-md-4">
        <span className="item_img">
          <img loading="lazy" src={product["images"][0]} className="w-100 small_img" />
        </span>
      </div>
      <div className="col-md-8">
        <div className="item_detail">
          <h3>{product[lang]["name"]} </h3>
          <p className="price">
            <span>
              {product.before_discount_price != null
                ? `${currency} ${parseFloat(
                    product.before_discount_price
                  ).toFixed(store.round_off)}`
                : ``}
            </span>
            <small>
              {currency}{" "}
              {parseFloat(product.retail_price).toFixed(store.round_off)}
            </small>
          </p>
        </div>
        <span className="show_cart">{renderControl()}</span>
        {show ? <ShowQuantity product={product} /> : ""}
      </div>
    </div>
  );
}

export default Products;
