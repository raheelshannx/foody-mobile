import React from "react";

function ShowCart() {
  return (
    <span className="show_cart">
      <a href="#/" title="Cart">
        <img loading="lazy" src="/assets/img/cart_icon.svg" />
      </a>
    </span>
  );
}

export default ShowCart;
