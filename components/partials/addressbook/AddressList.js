import React from "react";
import useTranslation from "../../../services/useTranslation";
function AddressList(props) {
    const { name, phone, edit} = useTranslation();
    const {Delete} = 'Delete';
    return (


        <li >
            <ul>
                <li>
                    {name} : <span>{data.name}</span>{" "}
                    <span className="pull-right">Home</span>
                </li>
                <li>{phone} :{data.phone}</li>
                <li>{data.address}</li>
                <li>
                    <a className="edit-btn" href>
                        {edit}
                    </a>
                    <a className="delete-btn ml-3" href>
                        {Delete}
                    </a>
                </li>
            </ul>
        </li>


    );
}
export default AddressList;