import React from "react";
import useTranslation from "../../../services/useTranslation";

import { useSelector } from "react-redux";


function PromotionCode() {
    const { promotioncode, enterpromotioncode } = useTranslation();
    const plugins = useSelector((state) => state.plugins);

    const handleClick = () => {
        let plugi = {
                        id: 21,
                        plugin_id: 7,
                        api_url: "https://discount.nextaxe.com/api/v1/",
                        api_password: "abc123!!",
                        api_secret: "vkFgftAeSAK6nSrajnw2BV5y1kb40Xb3xtyXBpN0",
                        api_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiYzAxOGFiZTUzMjFiMDRlODUxY2FhZTQzMTk3ZGJiZGQ5YTNlNGQ4MTc0MjZiN2E3YzFhOGMzMDFlZTA3ZGM2ZjBiYTVkZDM4YjhhNzRmYjMiLCJpYXQiOjE1OTQwMzY4NTAsIm5iZiI6MTU5NDAzNjg1MCwiZXhwIjoxNjI1NTcyODUwLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.hGrK8E8Y-M99EBi1pGlEPOMebmy6hqgQaDledfkWJpm88cWgpUF2z7a-k9lN7FccmHWIi-IZSlLneQfac5XoFNxZH8G82lC5QAAu4-rI4qKU0YKWBERjwoMjUvSQzcPM4Rk-o0itRiKNuNCABF2FTn5oH1ncLnUESppAWdD3XhLAKl_rOc_1vfnPBiIOI7HH_15uqvWcHcSeG28E9Es2SX0c3nYaZ21B_6mDzrKrHjbCvsvpd3UM9vCzUAEQTpsGomobYS4kHboGOO1bvSKESeEk0TcIinpYI06B_kQ7U4x4cl-RjHVhE4DLdbdWkvhGu60jmYWPRIh84GpPRyKDfr-NlRQueEMLQMZFs6zUPzpA1ixe-S5vW4wsa5tCOeLH0c8RkjUcn9EovJiIIsp80_RiA7yXL_A067dqejURxHMm4VsHUFld0grWuIlL82XXbvQRJjC4gbVvxXToBI048Hm64CQpROSOybNiFjVJTxTtF7ZNZyC1iFUurReh1e1ycIr3gKS9PBDHJzSCFUwT8lce6eJ2iX3IDhVVvNTZdyHOk1EibLtwqLiUafXMjEIDF5e-mbh6vAtIpi6O46a_vWZjOmH7-vgHeUptYFlhG1z4CDg8Lj9cg_gIveHIgQIuGRytptHmdzXO-DPuwsJJgSInsBZjcOlrMMMrzULGr9M",
                        plugin: {
                            id: 7,
                            name: "Discount",
                            slug: "discount"
                         }
                        }
        let voucher = $('#discount_voucher').val();
                        console.log(plugi,voucher);
                if(voucher.length > 0) {

                    let url = plugi.api_url;
                    let token = plugi.api_token;
                    var settings = {
                        "url": url + "vouchers/verify/" + voucher,                                  
                        "method": "GET",
                        "timeout": 0,
                        "headers": {
                            "Authorization": "Bearer " + token,
                            "Accept": "application/json"
                        },
                    };

                    let currency = 'KWD';

                    $.ajax(settings).done(function (response) {
                        console.log(response);
                        if(response.data == null) {
                            $('.voucher-error').html('Invalid coupon');
                            $('.voucher-error').removeClass('d-none');
                            $('.voucher-error').removeClass('text-success');
                            $('.voucher-error').addClass('text-danger');
                        }
                        if(response.status == 'valid') {
                            $('.voucher-error').html('Coupon Applied');
                            $('.voucher-error').removeClass('d-none');
                            $('.voucher-error').removeClass('text-danger');
                            $('.voucher-error').removeClass('text-warning');
                            $('.voucher-error').addClass('text-success');
                            $('#discount_voucher_code').val(voucher);
                            $('.discount_price').removeClass('d-none');
                            $('.discount_price_val').removeClass('d-none');
                            let grand_total = $('#g_total').html();
                            let total_amount = $('#tot_price').html();
                            console.log('Grand total=', grand_total);                         

                            if(response.data.discount_type == 1) {
                                let final_total = total_amount - response.data.discount_amount;
                                let discount_price = currency + " -" + response.data.discount_amount.toFixed(decimal_precision);
                                $('.discount_price_val').text(discount_price);
                                const subtotal = $('#subtotal').val()
                                $('#g_total').html((subtotal - response.data.discount_amount + parseFloat($('#shipping-rate').html())).toFixed(decimal_precision));
                                //$('#btn_discount_voucher').prop('disabled', true);
                            } else if(response.data.discount_type == 2) {

                                const percentage = response.data.discount_amount.toFixed(decimal_precision);
                                const subtotal = $('#subtotal').val();
                                const discount_price = (subtotal / 100 * percentage);

                                $('.discount_price_val').text(currency + " -" +parseFloat(discount_price).toFixed(decimal_precision));
                                $('#g_total').html((subtotal - discount_price + parseFloat($('#shipping-rate').html())).toFixed(decimal_precision));
                            }

                        } else if(response.status == 'expired') {
                            $('.voucher-error').html('Coupon expired');
                            $('.voucher-error').removeClass('text-danger');
                            $('.voucher-error').removeClass('text-success');
                            $('.voucher-error').addClass('text-warning');
                            $('#discount_voucher_code').val();
                        } else if(response.status == 'limit_reached') {
                            $('.voucher-error').html('Coupon usage limit reached');
                            $('.voucher-error').removeClass('text-danger');
                            $('.voucher-error').removeClass('text-success');
                            $('.voucher-error').addClass('text-warning');
                            $('#discount_voucher_code').val();
                        }
                    }).fail(function (jqXHR, textStatus) {
                        console.log(textStatus);
                    });
                }

    }
    const renderDiscountCode = () => {
        let is_true = false;
        plugins.map((plugin) => {
            if(plugin.plugin != null && plugin.plugin.slug == 'discount'){
                is_true = true;
            }
        })
        if(is_true){
            return (
                <div className="">
                  <h2 className="text-uppercase pt-4 pb-3">Promotion Code</h2>
                  <div className="form-row">
                    <div className="col-auto">
                      <input
                        id="discount_voucher"
                        type="text"
                        placeholder="Enter Promotion Code"
                        className="form-control col-xs-8"
                        name=""
                      />
                    </div>
                    <div className="col-auto ">
                      <button
                        className="btn btn-primary pull-right col-xs-4 m-1"
                        type="button"
                        onClick={() => handleClick()}
                        style={{
                          background: "#2D2F41",
                          backgroundColor: "#2D2F41",
                          borderColor: "#2D2F41",
                        }}
                      >
                        Apply
                      </button>
                    </div>
                  </div>
                </div>
            );
        }
    };

    return (
        <div>
            {renderDiscountCode()}
        </div>
    );
}
export default PromotionCode;
