function RadioButton(props){
    return(
        <li>
                {" "}
                <div className="custom-control custom-radio">
                  <input
                    type="radio"
                    id={props.id}
                    name="customRadio"
                    className="custom-control-input"
                    defaultChecked
                  />
                  <label
                    className="custom-control-label"
                    htmlFor={props.id}
                  >
                    {props.label}
                  </label>
                </div>
              </li>
    );
}
export default RadioButton;