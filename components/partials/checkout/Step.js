import React from "react";
function Step(props) {
    const styleClass = props.classBtn+" btn-circle";
    return (
        <div className="stepwizard-step">
            {/* note: disabled="disbaled" was removed because React doesnot support */}
            <a
                href={props.url}
                type="button"
                className={props.classBtn+" btn-circle"}
            >
                {props.number}
            </a>
            <p>{props.text}</p>
        </div>
    );
}
export default Step;