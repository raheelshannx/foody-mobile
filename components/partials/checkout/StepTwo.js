import CheckoutInfo from "./CheckoutInfo";
import SearchAddress from "./SearchAddress";
import LocationMap from "./LocationMap";
import FormGroup from "../../common/FormGroup";
import SubmitButton from "./SubmitButton";
import useTranslation from "../../../services/useTranslation";
import { Formik } from 'formik';
import { useDispatch } from "react-redux";
import { setStep } from "../../../redux/actions/checkoutStepAction";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";



function StepTwo() {
  const { area_name, block, street_number, house_or_apartment_no, Avenue, Special_Direction , pickup_address , billing_address , Precise_faster , required} = useTranslation();
  const [holder, setHolder] = useCheckoutDataHolder();
  const dispatch = useDispatch();
  const proceed = "Proceed";

  const [areaname, setAreaName] = useState();
  const [latlong, setlatlong] = useState("{}");
  const [blockname, setBlock] = useState();
  const [streetnumber, setStreetNumber] = useState();
  const [housenumber, setHouseNumber] = useState();
  const [avanue, setAvanue] = useState();
  const [direction, setDirection] = useState();

  useEffect(() => {
    setAreaName(holder.areaname);
    setBlock(holder.blockname);
    setStreetNumber(holder.streetnumber);
    setHouseNumber(holder.housenumber);
    setAvanue(holder.avanue);
    setDirection(holder.direction);
    if (holder.pickup) {
      setlatlong(JSON.stringify({ lat: parseInt(holder.outlet.latitude), lng: parseInt(holder.outlet.longitude) }));
    }
  });
  const getLatLng = () => {
  }
  console.log(holder);

  const getCurrentLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        position => {
          setlatlong(
            JSON.stringify({
              lat: parseInt(position.coords.latitude),
              lng: parseInt(position.coords.longitude)
            }));
        }
      )
    } else {
      error => console.log(error)
    }
  }
  const setLocation=(obj)=>{
    setlatlong(
      JSON.stringify(obj));
  }
  return (
    <div className="row setup-content" id="step-2">
      <div className="col-xs-6 ">
        <div className="col-md-12">

          {holder.pickup == true
            ?
            <span>
              <CheckoutInfo title={pickup_address}/> 
              <span>
                <p>
                  {holder.outlet.street_1},
                  {holder.outlet.outlet_city.city},
                  {holder.outlet.country}
                </p>
              </span>
            </span>
            :
            <span>
              <CheckoutInfo title={billing_address} subTitle={Precise_faster} /> 
              <SearchAddress />
            </span>
          }

          <LocationMap latlong={latlong} />
          <Formik
            initialValues={{ areaname: areaname, block: blockname, streetnumber: streetnumber, housenumber: housenumber, avanue: avanue, direction: direction }}
            validate={values => {
              const errors = {};

              if (!values.areaname) {
                errors.areaname = required;
              }
              if (!values.block) {
                errors.block = required;
              }
              if (!values.streetnumber) {
                errors.streetnumber = required;
              }
              if (!values.housenumber) {
                errors.housenumber = required;
              }
              if (!values.avanue) {
                errors.avanue = required;
              }
              if (!values.direction) {
                errors.direction = required;
              }
              return errors;

            }}
            onSubmit={(values, { setSubmitting, resetForm }) => {
              setTimeout(() => {

                holder.areaname = values.areaname;
                holder.block = values.block;
                holder.streetnumber = values.streetnumber;
                holder.housenumber = values.housenumber;
                holder.avanue = values.avanue;
                holder.direction = values.direction;
                setHolder(holder);
                console.log('holder', holder);
                setSubmitting(false);
                dispatch(setStep(3));
              }, 400);
              resetForm();
            }}
          >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting,
              /* and other goodies */
            }) => holder.pickup == false ? (
              <form className="contactus" onSubmit={handleSubmit}>

                  <div className="form-group">
                    <label className="control-label">
                      {area_name} <em>*</em>
                    </label>
                    <input
                      id="edit_area"
                      maxLength={100}
                      type="text"
                      className="form-control"
                      placeholder={area_name}
                      name="areaname"
                      onChange={handleChange} 
                      onBlur={handleBlur} 
                      value={values.areaname} 
                      hint={area_name}
                    />
                    {console.log(values.areaname)}
                   <p className="text-danger">{errors.areaname && touched.areaname && errors.areaname}</p>
                  </div>

                  <div className="form-group">
                    <label className="control-label">
                       {block}<em>*</em>
                    </label>
                    <input
                      id="block"
                      maxLength={100}
                      type="text"
                      className="form-control"
                      placeholder={block}
                      name="block"  
                      onChange={handleChange} 
                      onBlur={handleBlur} 
                      value={values.block} 
                      hint={block}
                    />
                    <p className="text-danger">{errors.block && touched.block && errors.block}</p>
                  </div>
                  <div className="form-group">
                    <label className="control-label">
                      {street_number} <em>*</em>
                    </label>
                    <input
                      id="street"
                      maxLength={100}
                      type="text"
                      className="form-control"
                      placeholder={street_number}
                       name="streetnumber"  
                       onChange={handleChange} 
                       onBlur={handleBlur} 
                       value={values.streetnumber} 
                       hint={street_number}
                    />
                    <p className="text-danger">{errors.streetnumber && touched.streetnumber && errors.streetnumber}</p>

                </div>

                  <div className="form-group">
                    <label className="control-label">
                      {house_or_apartment_no} <em>*</em>
                    </label>
                    <input
                      id="house_detail"
                      maxLength={100}
                      type="text"
                      className="form-control"
                      placeholder={house_or_apartment_no}
                      name="housenumber"  
                      onChange={handleChange} 
                      onBlur={handleBlur} 
                      value={values.housenumber} 
                      hint={house_or_apartment_no}
                    />
                    <p className="text-danger">{errors.housenumber && touched.housenumber && errors.housenumber}</p>

                  </div>
                  <div className="form-group">
                    <label className="control-label">
                      {Avenue}<em>*</em>
                    </label>
                    <input
                      maxLength={100}
                      type="text"
                      className="form-control"
                      placeholder={Avenue}
                      name="avanue" 
                      onChange={handleChange} 
                      onBlur={handleBlur} 
                      value={values.avanue} 
                      hint="Avenue"
                    />
                    <p className="text-danger">{errors.avanue && touched.avanue && errors.avanue}</p>

                  </div>
                  <div className="form-group">
                    <label className="control-label">
                      {Special_Direction}<em>*</em>
                    </label>
                    <textarea
                      maxLength={100}
                      rows={4}
                      className="form-control"
                      placeholder={Special_Direction}
                      defaultValue={" "}
                      name="direction" 
                      onChange={handleChange} 
                      onBlur={handleBlur} 
                      value={values.direction} 
                      hint={Special_Direction}
                    />
                    <p className="text-danger">{errors.direction && touched.direction && errors.direction}</p>

                </div>

                <button
                  className="btn btn-primary nextBtn btn-lg pull-right"
                  type="submit"
                  disabled={isSubmitting}
                >
                  {proceed}
                </button>

              </form>
            ) :

                <button
                  onClick={() => dispatch(setStep(3))}
                  className="btn btn-primary nextBtn btn-lg pull-right"
                  type="submit"
                >
                  {proceed}
                </button>
            }
          </Formik>
        </div>
      </div>
    </div>
  );
}
export default StepTwo;