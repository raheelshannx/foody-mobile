import Link from "next/link";
import useTranslation from "../../../services/useTranslation";

function SubmitButton() {
    // const { Review_Order } = useTranslation();
    return (
        <Link href="/revieworder">
            <a href="#"
                className="btn btn-primary btn-lg pull-left"
                type="submit"
            >
                {Review_Order}
        </a>
        </Link>
    );
}
export default SubmitButton;