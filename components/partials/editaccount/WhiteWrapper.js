import FormGroup from "../../common/FormGroup";
import useTranslation from "../../../services/useTranslation";
import { Formik } from 'formik';
import networkService from "../../../services/networkService";
import urlService from "../../../services/urlService";
import { useEffect, useState } from "react";

function WhiteWrapper() {
  const { name , phone , email , Update , required , invalid_email } = useTranslation();
  const [values, setValues] = useState(false);
  const onSubmitApi = async (values) => {
    let data = {
      client_id: 11,
      client_secret: 'E3WbxaE14ub0GG2GwWSp1NtPpTwUQYcPotcOC1p0',
      username: values.email,
      password: values.password,
      grant_type: 'password'
    };
    const response = await networkService.post(urlService.postAccountUpdate,data);
    if (response != null && response.IsValid) {
    orders = response.Payload;
    }
    console.log(response);
    }
    useEffect(() => {
      //setSetup(props);
      setInitialize();
    });
    const setInitialize = async()=>{
      const response = await networkService.get(urlService.getAccountEdit);
      if (response != null && response.IsValid){
      console.log(response);
      setValues(response.payload);  }
      }
  return (
    <div className="white_wrapper cart">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <br />
            <Formik
              initialValues={{ name: '', mobile: '', email: '',area:'' }}
              validate={values => {
                const errors = {};
                if (!values.name) {
                  errors.name = required;
                }
                if (!values.mobile) {
                  errors.mobile = required;
                }
                if (!values.email) {
                  errors.email = required;
                } else if (
                  !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                  errors.email = invalid_email;
              }
                  return errors;
              }}
              onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                  onSubmitApi(values);
                //   const response = networkService.post(urlService.postWishlistToggle, data);
                //   console.log(response);

                //   // alert(JSON.stringify(values, null, 2));
                //   setSubmitting(false);
                }, 400);
              }}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                /* and other goodies */
              }) => (
                  <form onSubmit={handleSubmit}>
                    <FormGroup label={name} name="name" onChange={handleChange} onBlur={handleBlur} value={values.name} hint={name} />
                    <p className="text-danger">{errors.name && touched.name && errors.name}</p>

                    <FormGroup label={phone} name="mobile" onChange={handleChange} onBlur={handleBlur} value={values.mobile} hint={phone} />
                    <p className="text-danger">{errors.mobile && touched.mobile && errors.mobile}</p>

                    <FormGroup label={email} name="email" onChange={handleChange} onBlur={handleBlur} value={values.email} hint={email} />
                    <p className="text-danger">{errors.email && touched.email && errors.email}</p>

                    <button className="primary mt-3 mb-3" type="submit" disabled={isSubmitting}>{Update}</button>
                  </form>
                )}
            </Formik>
          </div>
        </div>
      </div>
    </div>

  );
}
export default WhiteWrapper;