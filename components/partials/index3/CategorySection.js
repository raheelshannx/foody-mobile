import React from "react";
import Products from "../Home/Products";
import useLanguage from "../../../services/useLanguage";
import SubCategory from "./SubCategory";

function CategorySection({ categories, products, parent }) {
    const lang = useLanguage();

    const renderCategoryOrProducts = (category, products) => {
        let parentAccordion = `#accordion${category.id}`;
        if (category['children'].length > 0) {
            console.log(category.en, 'Children Found');
        } else {
            console.log(category.en, 'Children Not Found');
        }
        return (
            category['children'].length > 0
                ? <CategorySection parent={parentAccordion} categories={category['children']} products={products} />
                : products.map(product => {
                    return (
                        <Products product={product} />
                    )
                })
        )

    }

    const render = (categories, products, parent) => {
        console.log(categories);
        if (categories != undefined) {
            return (
                categories.map(category => {
                    let ac = `ac${category.id}`;
                    let filteredProducts = products.filter(prod => {
                        return prod.category_id == category.id;
                    });
                    if (filteredProducts.length > 0) {
                        let parentAccordion = parent;
                        return (
                            <div className="panel">
                                <div
                                    className="card-header collapsed mb-2"
                                    data-toggle="collapse"
                                    href={`#${ac}`}
                                >
                                    <a className="card-title">
                                        {category[lang]}<span>({filteredProducts.length})</span>
                                    </a>
                                </div>
                                <div
                                    id={ac}
                                    className="card-body collapse "
                                    data-parent={parentAccordion}
                                >
                                    {renderCategoryOrProducts(category, filteredProducts)}
                                </div>
                            </div>

                        );
                    }
                })
            );
        } else {
            return <div>Nothing found</div>
        }
    }

    return render(categories, products, parent);
}

export default CategorySection;
