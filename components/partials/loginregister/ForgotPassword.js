import React from "react";
import useTranslation from "../../../services/useTranslation";

function ForgotPassword() {
    const {forgot_password} = useTranslation();
    return (
        <a
            href="#"
            data-toggle="modal"
            data-target="#exampleModalCenter"
        >
            {forgot_password}
</a>
    );
}

export default ForgotPassword;
