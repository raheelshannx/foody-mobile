import useLanguage from "../../../services/useLanguage";
import useCurrency from "../../../services/useCurrency";

function ProductItem({ product }) {
    const lang = useLanguage();
    const currency = useCurrency();
    // console.log(product);
    return (
        <div>
            <div className="row mt-3 mb-3 align-items-center">
                <div className="col-md-4">
                    <span className="item_img">
                        <img
                            loading="lazy"
                            src={product.image}
                            className="w-100"
                        />
                    </span>
                </div>
                <div className="col-md-8">
                    <div className="item_detail">
                        <h3>{product[lang].name}</h3>
                        <p className="price">
                            {product.before_discount_price == null ? '' : <span>{`${currency} ${product.before_discount_price}`}</span>}
                            <small>{`${currency} ${product.retail_price}`} </small>
                        </p>
                    </div>
                </div>
            </div>
            <hr />
        </div>
    );
}
export default ProductItem;