import React from "react";

function CartItem({order}) {
    return (
        <div className="d-flex p-3 mt-2  justify-content-between align-items-center">
            <img loading="lazy" src="assets/img/chicken.jpg" width={50} />
            <p className="mb-0">Chicken Tikka</p>
            <strong>KD : 200</strong>
        </div>
    );
}

export default CartItem;