import useTranslation from "../../../services/useTranslation";

function CartTotal() {
  const { sub_total , Delivery , total } = useTranslation();
  return (
    <div className="d-flex p-3 mt-2 mb-5 justify-content-between cart_total">
      <span>
        <p>{sub_total}</p>
        <p>{Delivery}</p>
        <p>
          <b>{total}</b>
        </p>
      </span>
      <span className="text-right">
        <p>KWD 18.00</p>
        <p>KWD 2.00</p>
        <p>
          <b>KWD 20.00</b>
        </p>
      </span>
    </div>
  );
}
export default CartTotal;