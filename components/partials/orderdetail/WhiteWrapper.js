import OrderStatus from "./OrderStatus";
import OrderList from "./OrderListItem";
import CartItem from "./CartItem";
import CartTotal from "./CartTotal";
import SubmitButton from "./SubmitButton";
import useTranslation from "../../../services/useTranslation";
import { useEffect } from "react";
import networkService from "../../../services/networkService";
import urlService from "../../../services/urlService";

function WhiteWrapper({ id }) {
  const { delivered_to, payment_method, Order_Items } = useTranslation();

  let order = {};
  const initialize = async () => {
    const response = await networkService.get(`${urlService.getOrderList}/${id}`);
    if (response.IsValid) {
      order = response.Payload;
    }
    console.log(response);
  }
  useEffect(() => {
    initialize();
  });


  return (
    <div className="white_wrapper cart">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <ul className="ul-order-detail-list pt-3">
              <OrderStatus order_no={order.order_number} status={order.status} time={order.order_time} />
              <OrderList title={delivered_to} detail={order.address.address} />
              <OrderList title={payment_method} detail={order.payment_method.name} />
            </ul>
            <hr />
            <div className="item-box pt-3">
              <h3>{Order_Items}</h3>
              {order['order_items'].map(order => {
                return <CartItem order={order} />;
              })}
            </div>
            <hr />
            <CartTotal />
            <SubmitButton />
          </div>
        </div>
      </div>
    </div>

  );
}
export default WhiteWrapper;