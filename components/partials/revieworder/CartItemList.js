import React from "react";
import CartItemName from "./CartItemName";
import Quantity from "./Quantity";
import { useSelector } from "react-redux";
import useLanguage from "../../../services/useLanguage";

function CartItemList() {
  const cart = useSelector((state) => state.cart);
  const lang = useLanguage();
  return (
    <ul className="cart_list_items pt-2 pb-2 check">
      {cart.items.map((item) => {
        return (
          <li>
            <CartItemName name={item[lang].name} />
            <Quantity qty={item.quantity} ttl={item.total} />
          </li>
        );
      })}
    </ul>
  );
}

export default CartItemList;
