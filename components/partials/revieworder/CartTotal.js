import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import useTranslation from "../../../services/useTranslation";
import useCheckoutDataHolder from "../../../services/useCheckoutDataHolder";
import useCurrency from "../../../services/useCurrency";

function CartTotal() {
  const { sub_total, Delivery, total, discount } = useTranslation();
  const cart = useSelector((state) => state.cart);
  const store = useSelector((state) => state.store);
  const [holder, setHolder] = useCheckoutDataHolder();
  const currency = useCurrency();
  const [cartTotal, setCartTotal] = useState(cart.total);

  let is_discount = false;
  let cop_discount = holder.cop_discount;

  if(cop_discount > 0){
      is_discount = true;
  }else{
      cop_discount = 0;
  }
  console.log(cop_discount,holder);

  const renderShippingTitle = () => {
    if (holder.shippingMethod != null) {
      return (
        <>
          <p>{Delivery}</p>
          <p>{"Time"}</p>
          {cop_discount > 0 ? <p>{discount}</p> : '' }
        </>
      );
    }
  };

  const renderShipping = () => {
    if (holder.shippingMethod != null) {
      return (
        <>
          <p>
            {currency} {holder.shippingMethod.rate.toFixed(store.round_off)}
          </p>
          <p>{holder.shippingMethod.delivery_time}</p>
          {cop_discount > 0 ? <p> {currency} - {cop_discount}</p> : '' }
        </>
      );
    }
  };

  const handleClick = () => {
      
  }

  useEffect(() => {
    let t = cartTotal;
    if (holder.shippingMethod != null) {
      let t = parseFloat(cart.total) + parseFloat(holder.shippingMethod.rate) - parseFloat(cop_discount);
      setCartTotal(t.toFixed(store.round_off));
    }
  });

  return (
    <div>
      <div className="d-flex p-4 grey mt-2 mb-5 justify-content-between cart_total">
        <span>
          <p>{sub_total}</p>
          {renderShippingTitle()}
          <p>
            <b>{total}</b>
          </p>
        </span>
        <span className="text-right">
          <p>
            {currency} {cart.total}
          </p>
          {renderShipping()}
          <p>
            <b>
              {currency} {cartTotal}
            </b>
          </p>
        </span>
      </div>
    </div>
  );
}

export default CartTotal;
