import React from "react";
import SubTitles from "../../common/SubTitles";
import useTranslation from "../../../services/useTranslation";

function GeneralInfo({ uName, uEmail, uPhone }) {
    const { general_info, name, phone, email } = useTranslation();
    return (
        <div>
            <SubTitles title={general_info} />
            <table className="table">
                <tbody>
                    <tr>
                        <th>{name} : </th>
                        <td>{uName}</td>
                    </tr>
                    <tr>
                        <th>{phone} : </th>
                        <td>{uPhone}</td>
                    </tr>
                    <tr>
                        <th>{email} : </th>
                        <td>{uEmail}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default GeneralInfo;
