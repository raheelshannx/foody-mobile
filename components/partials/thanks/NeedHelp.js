import React from "react";
import useTranslation from "../../../services/useTranslation";

function NeedHelp() {
    const {Need_Help} = useTranslation();
    return (
        <a href="#/" className="need_help">
            {Need_Help}
  </a>
    );
}

export default NeedHelp;
