import React from "react";
function Script() {
  return (
    <div>
      <script src="/assets/js/jquery.min.js" />
      <script src="/assets/js/bootstrap.bundle.js" />
      <script src="assets/js/scripts.js" />
      <script src="assets/js/toastr.js"/>
      <script src="/assets/js/main.js" />
      <script src="https://code.iconify.design/1/1.0.6/iconify.min.js"></script>
    </div>
  );
  {/* <script>
});

 */}
  {/* </script> */ }
}
  export default Script;
