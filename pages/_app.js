import React, { useEffect } from "react";

import PageHead from "../components/pagehead";
import Footer from "../components/footer";
import Sub_Footer from "../components/subfooter";
import MySideNav from "../components/sidenav";
import NewsLetter from "../components/newsletter";
import Script from "../components/script";
import Header from "../components/common/Header";
import { Provider } from "react-redux";
import store from "../redux/index";

export default function MyApp({ Component, pageProps }) {
  const Layout = Component.Layout ? Component.Layout : React.Fragment;

  useEffect(() => {
    if (typeof window === "undefined") {
      global.window = {};
    }
  }, []);

  return (
    <Provider store={store}>
      <Layout>
        <PageHead />
        <div className="content">
          <Header />
          <MySideNav />
          <Component {...pageProps} />
          <NewsLetter />
          <Footer />
          <Sub_Footer />
        </div>
        <Script />
      </Layout>
    </Provider>
  );
}
