import FormGroup from "../components/common/FormGroup";
import TextArea from "../components/common/TextArea";
import Maps from "../components/partials/contactus/Map";
import SubHeader from "../components/common/SubHeader";
import RoundedButton from "../components/common/RoundedButton";
import useTranslation from "../services/useTranslation";
import { Formik } from 'formik';
import urlService from "../services/urlService";
import networkService from "../services/networkService";

 var message;
function ContactUs() {
  const { name, phone, email, subject, tell_us, send_message , contact_us , invalid_email , required , submit} = useTranslation();

  const onSubmitApi = async (values) => {
    const response = await networkService.post(urlService.postContactUs,values);
    if (response.IsValid ) {
      let res = response.Payload;
    }
      console.log('response',response);
  }
  return (
    <div>
      <SubHeader title={contact_us} />
      <div className="white_wrapper cart">
        <div className="container">
          <div className="row">
            <div className="col-md-12 mt-3">
              <Formik
                initialValues={{ name: '', email: '', phone: '', subject: '', message: '' }}
                validate={values => {
                  const errors = {};
                  if (!values.email) {
                    errors.email = required;
                  } else if (
                    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                  ) {
                    errors.email = invalid_email;
                  }
                  if (!values.name) {
                    errors.name = required;
                  }
                  if (!values.phone) {
                    errors.phone = required;
                  }
                  if (!values.subject) {
                    errors.subject = required;
                  }
                  if (!values.message) {
                    errors.message = {required};
                  }
                  return errors;
                }}
                onSubmit={(values, { setSubmitting ,resetForm}) => {
                  setTimeout(() => {
                    onSubmitApi(values)

                    const data = {
                      name : values.name,
                      email : values.email,
                      phone : values.phone , 
                      message : values.message,

                      ///etc
                    }
                  console.log(data)
                    

                    

                     // alert(JSON.stringify(values, null, 2));
                    setSubmitting(false);

                  }, 400);
                  resetForm();
                }}
              >
                {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                  /* and other goodies */
                }) => (
                    <form className="contactus" onSubmit={handleSubmit}>

                      <FormGroup label={name} name="name" onChange={handleChange} onBlur={handleBlur} value={values.name} hint={name} />
                      <p className="text-danger">{errors.name && touched.name && errors.name}</p>

                      <FormGroup label={phone} type="number" name="phone" onChange={handleChange} onBlur={handleBlur} value={values.phone} hint={phone} />
                      <p className="text-danger">{errors.phone && touched.phone && errors.phone}</p>

                      <FormGroup label={email} name="email" onChange={handleChange} onBlur={handleBlur} value={values.email} hint={email} />
                      <p className="text-danger">{errors.email && touched.email && errors.email}</p>

                      <FormGroup label={subject} name="subject" onChange={handleChange} onBlur={handleBlur} value={values.subject} hint={subject} />
                      <p className="text-danger">{errors.subject && touched.subject && errors.subject}</p>

                      <TextArea label={tell_us} name="message" onChange={handleChange} onBlur={handleBlur} value={values.message} hint={tell_us} />
                      <p className="text-danger">{errors.message && touched.message && errors.message}</p>

                      <button className="primary mt-3 mb-3" type="submit" disabled={isSubmitting}>
                        {submit}
                            </button>

                    </form>
                  )}
              </Formik>
              <Maps />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ContactUs;