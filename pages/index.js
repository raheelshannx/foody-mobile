import Slider from "../components/partials/Home/Slider";
import VendorDetail from "../components/partials/Home/VendorDetail";
import SearchFields from "../components/partials/Home/SearchFields";
import CategoriesArea from "../components/partials/Home/CategoriesArea";
import PopUp from "../components/partials/Home/PopUp";
import DetailPopUp from "../components/partials/Home/DetailPopUp";
import FixedButtonArea from "../components/FixedButtonArea";
import networkService from "../services/networkService";
import urlService from "../services/urlService";
import useSetup from "../services/useSetup";
import { useState, useEffect } from "react";
import Home2Products from "../components/partials/Home/Home2Products";
import Home3Products from "../components/partials/Home/Home3Products";
import Home4Products from "../components/partials/Home/Home4Products";
import Home5Products from "../components/partials/Home/Home5Products";
import SearchModel from "../components/common/SearchModel";

export default function Home(props) {
  const [initialize, setInitialize] = useState(false);
  const [setup, setSetup] = useSetup(props);
  const { banners, products } = props;
  const type = 1;

  useEffect(() => {
    //setSetup(props);
    setInitialize(true);
  }, [false]);

  const selectLayout = (type) => {
    if (type == 1) {
      return <CategoriesArea products={products} />;
    } else if (type == 2) {
      return <Home2Products products={products} />;
    } else if (type == 3) {
      return <Home3Products products={products} />;
    }else if(type == 4){
      return <Home4Products products={products}/>
    }else if(type == 5){
      return <Home5Products products={products}/>

    }
  };

  return (
    <div>
      <Slider banners={banners} />
      <VendorDetail />
      <hr />
      <SearchFields />

      {
        selectLayout(type)
      }

      {/* 
<Home2Products/>
     <Home3Products products={products}/>

     <Home4Products/>

     <Home5Products/> */}
      <DetailPopUp />
      <FixedButtonArea />
    </div>
  );
}

export async function getStaticProps() {
  let data = {
    banners: [],
    products: []
  };

  const response = await networkService.get(urlService.getSetup);

  if (response.IsValid) {
    data = { ...response.Payload };
  }

  // Get external data from the file system, API, DB, etc.
  const homeResponse = await networkService.get(urlService.getHome);

  if (homeResponse.IsValid) {
    const { banners, products } = homeResponse.Payload;

    data.banners = banners;
    data.products = products;
  }
  return {
    props: data,
  };
}
