import Header from "../components/common/Header"
import Slider from "../components/partials/Home/Slider"
import VendorDetail from "../components/partials/Home/VendorDetail";
import SearchFields from "../components/partials/Home/SearchFields";
import TopCategory from "../components/partials/Home/TopCategory";
import ProductGridItem from "../components/partials/Home/ProductGridItem";
import PopUp from "../components/partials/Home/PopUp";
import DetailPopUp from "../components/partials/Home/DetailPopUp";
import FixedButtonArea from "../components/FixedButtonArea";

export default function Home() {
  return (
    <div>
      <div>

        <Header />
        {/* <Slider /> */}
        <VendorDetail />
        <hr />
        <SearchFields />
        {/* Products Categories */}
        <div className="home2_products_area">
          <div className="products_row">
            <TopCategory />

            {
              [1, 2, 3, 4].map(product => {
                return (
                  <div className="product_scroller">
                    <ul className="inner_pro">

                      {
                        [1, 2, 3, 4].map(product => {
                          return (
                            <ProductGridItem />
                          )
                        })
                      }

                    </ul>
                    <hr />
                  </div>
                )
              })
            }

          </div>
        </div>
        <PopUp />
        <DetailPopUp />
        <FixedButtonArea />
      </div>
    </div>
  );
}