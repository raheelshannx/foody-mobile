import SubHeader from "../components/common/SubHeader";
import WhiteWrapper from "../components/partials/orderhistory/whiteWrapper";
import useTranslation from "../services/useTranslation";
import urlService from "../services/urlService";
import networkService from "../services/networkService";
import { useEffect } from "react";

export default function OrderHistory() {
  let orders = [];
  const initialize = async () => {
    const response = await networkService.get(urlService.getOrderIndex);
    if (response.IsValid) {
      orders = response.Payload.data;
    }
    console.log(response);
  }
  useEffect(() => {
    initialize();
  });

    const { order_history } = useTranslation();
    return (
        <div>
    
            <SubHeader title={order_history} />
            <WhiteWrapper orders={orders}/>
        </div>
    );
}