import SubHeader from "../components/common/SubHeader";
import SubTitles from "../components/common/SubTitles";
import CartItemList from "../components/partials/revieworder/CartItemList";
import GeneralInfo from "../components/partials/revieworder/GeneralInfo";
import DeliveryInfo from "../components/partials/revieworder/DeliveryInfo";
import PaymentInfo from "../components/partials/revieworder/PaymentInfo";
import SpecialNotes from "../components/partials/revieworder/SpecialNotes";
import CartTotal from "../components/partials/revieworder/CartTotal";
import useTranslation from "../services/useTranslation";
import useCheckoutDataHolder from "../services/useCheckoutDataHolder";
import { useSelector, useDispatch } from "react-redux";

import urlService from "../services/urlService";
import networkService from "../services/networkService";


export default function ReviewOrder() {
  const { Payment_Info, shipping_method, special_notes , min_order_amount_of , Place_Order } = useTranslation();

  const {
    Review_Order,
    Order_Items,
    Delivery_Info,
    Avenue,
    apartment_or_office,
    street_number,
    area_name,
    city,
    country,
  } = useTranslation();
  const [holder] = useCheckoutDataHolder();
  const { name, email, phone, paymentMethod, shippingMethod, outlet, avanue, housenumber, streetnumber, areaname } = holder;

  const webSetting = useSelector((state) => state.webSetting);
  const outlets = useSelector((state) => state.outlets);
  const cart = useSelector((state) => state.cart);


  const placeOrder = async() => {
    let new_cart =[];
    cart.items.map((item) => (
        new_cart.push({
            'product_id' : item.product_id,
            'qty' : item.quantity,
            'price' : item.retail_price,
            'total' : item.total,
            'variant_id' : item.variant_id,
        })

    ))
      console.log(JSON.stringify(new_cart))

      let outlet_id =  outlets[0].id;
      let shipping_rate =  holder.shippingMethod.rate;
      
      if(holder.area != null){
         outlet_id = holder.area.outlet_id;
         shipping_rate =  holder.shippingMethod.rate;
      }
      let city = holder.city != undefined ?  ', ' +  holder.city : '';
      let country = holder.country  != undefined ?  ', ' + holder.country : '';
      let address = 
          holder.avanue + ', ' + 
          holder.housenumber + ', ' + 
          holder.streetnumber + ', ' + 
          holder.block + ', ' + 
          holder.areaname +
          city +
          country;

    const checkout_obj = {
        address_id: 0,
        shipping: holder.shippingMethod.id,
        payment_method: holder.paymentMethod.id,
        country: '',
        company: '',
        address: address,
        city: '',
        county: '',
        avenue: holder.avanue,
        house_detail: holder.housenumber,
        street: holder.streetnumber,
        block: holder.block,
        area: holder.areaname,

        name: holder.name,
        email: holder.email,
        phone: holder.phone,
        user_firstName: holder.name,
        user_lastName: '',
        user_mobile: holder.phone,
        user_email: holder.email,
        delivery_date: '',
        delivery_time: holder.shippingMethod.delivery_time,
        re_subscription_days: '',
        applyReward: '',
        reward_value: '',
        guest_login: '',
        notes: holder.direction,
        outlet_id: outlet_id ,
        shipping_rate: holder.shippingRate > 0 ? holder.shippingRate : holder.shippingMethod.rate,
        is_pickup: holder.pickup,
        cart: JSON.stringify(new_cart),
    }
    // console.log('holder',holder,checkout_obj);

    const response = await networkService.post(urlService.postOrderSave,checkout_obj);

      // console.log('ted1',response);
    if(response != null && response.IsValid){
      // console.log('ted',response);
      if(response.Payload.payment_url != null){
        const win = window.open(response.Payload.payment_url, '_blank');
        if (win != null) {
          win.focus();
        }
      }
      if(response.Payload.success){
        const win = window.open('thanks?order_number='+response.Payload.order_number);
        if (win != null) {
          win.focus();
        }
      }
    }
  }
    const renderPaymentMethod = () => {
    if (paymentMethod != null) {
      return (
        <div>
          <h2 className="text-uppercase pt-4 pb-3"> {Payment_Info} </h2>
          <p>
            <span
              className="iconify checked_icon"
              data-icon="ant-design:check-circle-filled"
            />
            {paymentMethod.name}
          </p>
          <hr />
        </div>
      );
    }
  };

  const renderShippingInfo = () => {
    if (!holder.pickup && shippingMethod != null) {
      return (
        <div>
          <h2 className="text-uppercase pt-4 pb-3"> {shipping_method} </h2>
          <p>
            <span
              className="iconify checked_icon"
              data-icon="ant-design:check-circle-filled"
            />
            {shippingMethod.name} ({shippingMethod.rate})
          </p>
          <hr />
        </div>
      );
    }
  };

  const renderPickupOrDelivery = () => {
    if (holder.pickup) {
      return (
        <div>
          <h3 className="mt-4 mb-2">Pickup</h3>
          <div class="mt-3">
            <p>
              {outlet.street_1}, {outlet.city}, {outlet.country}
            </p>
            <hr />
          </div>
        </div>
      );
    } else {
      return (
        <div>
          <h3 className="mt-4 mb-2">{Delivery_Info}</h3>
          <div className="mt-3">
            <h6>{Avenue}</h6>
            <p>{avanue}</p>
            <hr />
          </div>
          <div className="mt-3">
            <h6>{apartment_or_office}</h6>
            <p>{housenumber}</p>
            <hr />
          </div>
          <div className="mt-3">
            <h6>{street_number}</h6>
            <p>{streetnumber}</p>
            <hr />
          </div>
          <div className="mt-3">
            <h6>{area_name}</h6>
            <p>{areaname}</p>
            <hr />
          </div>
          <div className="mt-3">
            <h6>{city}</h6>
            <p>{holder.city}</p>
            <hr />
          </div>
          <div className="mt-3">
            <h6>{country}</h6>
            <p>{holder.country}</p>
            <hr />
          </div>
        </div>
      );
    }
  };
  const renderMinOrder = () => {
    if (webSetting.min_order_value > cart.total) {
      return (
        <button
          style={{ width: "100%", height: "60px", "z-index": "1" }}
          className="btn btn-danger nextBtn btn-lg pull-right mt-4 mb-2"
        >
          {min_order_amount_of} {webSetting.min_order_value}{" "}
          {webSetting.default_currency}
        </button>
      );
    }
  };
  const renderPLaceOrder = () => {
    if (webSetting.min_order_value < cart.total) {
      return (
        <div className="row setup-content">
          <div className="col-md-12">
          <button
            className="btn btn-primary nextBtn btn-lg pull-right"
            type="button"
            onClick={()=>placeOrder()}
            //disabled={isSubmitting}
          >
           {Place_Order}
          </button>
        </div>
        </div>
      );
    }
  };
  return (
    <div>
      <SubHeader title={Review_Order} />

      <div className="white_wrapper cart">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <GeneralInfo uName={name} uEmail={email} uPhone={phone} />
              <hr />
              {renderPickupOrDelivery()}
              {renderPaymentMethod()}
              {renderShippingInfo()}

              <div>
                <div className="mt-3">
                  <h6>{special_notes}</h6>
                </div>
                <p className="mt-3">{holder.direction}</p>
              </div>
              <hr />
              <SubTitles title={Order_Items} />
              <hr />
              <CartItemList />
            </div>
          </div>
        </div>
      </div>
      <CartTotal />

      {renderPLaceOrder()}
      {renderMinOrder()}
    </div>
  );
}
