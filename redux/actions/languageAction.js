//set into list action
export const addLanguage = (payload) => {
    return {
        type: 'ADD_LANGUAGE',
        payload
    }
}

export function setDefault(payload) {
    return {
        type: 'SET_LANGUAGE',
        payload
    }
};

export function getDefault() {
    return {
        type: 'GET_LANGUAGE',
    }
};