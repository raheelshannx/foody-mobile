const areaItem = (state, action) => {
    switch (action.type) {
        case 'ADD_AREA':
            return action.payload;
        default:
            return state;
    }
}

export default function areaReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_AREA':
            let item = state.find(item =>  item.id == action.payload.id);
            if (item == undefined) {
                return [
                    ...state,
                    areaItem(undefined, action)
                ];
            } else {
                return state;
            }
        default:
            return state;
    }
}