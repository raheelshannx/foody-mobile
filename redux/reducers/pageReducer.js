const pageItem = (state, action) => {
    switch (action.type) {
        case 'ADD_PAGE':
            return action.payload;
        default:
            return state;
    }
}


export default function pageReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_PAGE':
            let item = state.find(item =>  item.slug == action.payload.slug);
            if (item == undefined) {
                return [
                    ...state,
                    pageItem(undefined, action)
                ];
            } else {
                return state;
            }
        default:
            return state;
    }
}