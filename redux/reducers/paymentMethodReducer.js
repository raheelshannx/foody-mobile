const paymentMethodItem = (state, action) => {
    switch (action.type) {
        case 'ADD_PAYMENT_METHOD':
            return action.payload;
        default:
            return state;
    }
}

export default function paymentMethodReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_PAYMENT_METHOD':
            let item = state.find(item =>  item.slug == action.payload.slug);
            if (item == undefined) {
                return [
                    ...state,
                    paymentMethodItem(undefined, action)
                ];
            } else {
                return state;
            }
        default:
            return state;
    }
}