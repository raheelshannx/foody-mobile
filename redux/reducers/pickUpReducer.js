const initialState = { pickup : false , outlet : '', area : '' };

export default function pickUpReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_PICKUP":
      return action.payload;
    default:
      return state;
  }
}
