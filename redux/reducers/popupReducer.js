const initialState = {  };

export default function popupReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_POPUP":
      return action.payload;
      case "REMOVE_POPUP":
        return {};      
    default:
      return state;
  }
}
