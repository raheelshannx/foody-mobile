import React, { createContext, useState, useEffect } from "react";
import networkService from "./networkService";
import urlService from "./urlService";

export const ApplicationContext = createContext();

const ApplicationContextProvider = (props) => {
  const { pages, translations, categories, languages } = props;

  return (
    <ApplicationContext.Provider
      value={{ pages, translations, categories, languages }}
    >
      {props.children}
    </ApplicationContext.Provider>
  );
};
export default ApplicationContextProvider;
